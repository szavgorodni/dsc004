name := "dsc004"

version := "0.1"

scalaVersion := "2.11.11"


val sparkVersion = "2.2.0"

resolvers ++= Seq(

  "apache-snapshots" at "http://repository.apache.org/snapshots/",
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

)


libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion exclude("jline", "2.12"),
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.hadoop" % "hadoop-common" % "2.7.0" exclude ("org.apache.hadoop","hadoop-yarn-server-web-proxy"),
  "org.apache.spark" %% "spark-hive" % sparkVersion,
  "org.apache.spark" %% "spark-yarn" % sparkVersion,
  "com.github.kxbmap" %% "configs" % "0.4.4"
)