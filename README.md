# DOLLAR SHAVE CLUB EXCERCISE #3 #

This is Spark 2.2.0 Scala application that downloads and transforms external CSV file to the format specified in DSC Excercise #3  
The details of the setup and execution are below  
The application is designed to run in local mode on a single Windows box.  
This is done just to simplify the demonstration of the application.  
Very small changes are required to run in in cluster on unix  

### Prerequisites ###
Windows box has to have the following softwarte installed

* Java 1.8
* Scala 2.11
* SBT 0.13.15
* Spark 2.2.0
* winutils *- to emulate hadoop environement*

Very detailed instructions on the Spark installiton in Windows environement available below 

* [Spark On Windows #1](http://www.ics.uci.edu/~shantas/Install_Spark_on_Windows10.pdf)
* [Spark On Windows #2](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwi1__zP5pfWAhXKwlQKHXsgB8wQFggoMAA&url=https%3A%2F%2Fwww.ibm.com%2Fdeveloperworks%2Fcommunity%2Ffiles%2Fbasic%2Fanonymous%2Fapi%2Flibrary%2Fe5c0146d-f723-446b-9151-c31d4c56ed01%2Fdocument%2Fb41505ac-141b-45a2-84cd-1b6a8d5ae653%2Fmedia&usg=AFQjCNH19OEjmgi2RWAWclAbLsOz-0t4AQ)
* [Spark On Windows #3](http://www.eaiesb.com/blogs/?p=334)

The windows box has to have directory **E:\tmp** 
This is necesseray to store the output of the job in the app work directory

### How do I get set up and run? ###

Setup instrunction below assumes that app is installed on disk **E**, directory **E:\projects**  

* Clone the repo and change to the project root 
```bash
e:
cd  E:\projects
git clone https://szavgorodni@bitbucket.org/szavgorodni/dsc004.git
cd dsc004
```

* Compile and package 
```bash
# make sure that you in the project root directory
e:
cd  E:\projects\dsc004
# compile 
sbt compile
# package 
sbt package
# make sure that jar file is created
# make a note on the jar file name. it will be used to submit spark job
dir /A E:\projects\dsc004\target\scala-2.11\*.jar
# make sure that directory "E:\temp" exists
dir /A e:\tmp
```

* Run
```bash
# submit spark job

spark-submit --class com.sz.dsc004.DSC03 ^
--master local --deploy-mode client ^
--executor-memory 1g ^
--name dsc004  E:\projects\dsc004\target\scala-2.11\dsc004_2.11-0.1.jar

```
* The job execution steps and output: 

```bash
# this is typical output produced by Spark job 
# it can be used to understand the execution steps of the application

:: Starting Spark 2.2.0 app - DSC Exercise #3
:: running in local mode: local[*]
:: source: https://dsc-data-challenge.s3-us-west-2.amazonaws.com/pageViews.csv
:: setting up work directories: 
:: input directory: E:\tmp\dsc03\in
:: output directory: E:\tmp\dsc03\out
:: define Spark context: 
Using Spark's default log4j profile: org/apache/spark/log4j-defaults.properties
:: define CSV input parsing to DataSet: 
:: define Spark SQL query transformation: 
::write output to destination directory: 
::show a few lines of the output: 
+--------------------+-------------------+----------------+--------------------+--------------------+--------------------+
|            event_id|   collector_tstamp|   domain_userid|        page_urlpath|       next_event_id|        next_urlpath|
+--------------------+-------------------+----------------+--------------------+--------------------+--------------------+
|84123929-0b8c-472...|2016-01-01 05:32:26|00eb9bebcb2d7eac|       /how-it-works|                null|                null|
|7179ffd4-ceba-456...|2016-01-01 17:31:21|01ffe789f596ef32|              /login|a2499ffd-2b8b-40d...|                   /|
|a2499ffd-2b8b-40d...|2016-01-01 17:31:39|01ffe789f596ef32|                   /|df5ce6a9-046e-464...|/our-products/sha...|
|df5ce6a9-046e-464...|2016-01-01 17:31:43|01ffe789f596ef32|/our-products/sha...|eec31385-7fa2-490...|/your-box/add-pro...|
|eec31385-7fa2-490...|2016-01-01 17:31:43|01ffe789f596ef32|/your-box/add-pro...|eac9253b-750d-4ce...|/your-box/add-pro...|
|eac9253b-750d-4ce...|2016-01-01 17:31:46|01ffe789f596ef32|/your-box/add-pro...|616e96b6-725f-470...|/our-products/sha...|
|616e96b6-725f-470...|2016-01-01 17:31:48|01ffe789f596ef32|/our-products/sha...|17a3b4b7-824c-4c0...|       /our-products|
|17a3b4b7-824c-4c0...|2016-01-01 17:31:56|01ffe789f596ef32|       /our-products|                null|                null|
|b9806f97-eb6b-4f4...|2016-01-01 04:35:22|040b6f5e595e576a|             /blades|62292487-dbe2-4cc...|    /your-box/funnel|
|62292487-dbe2-4cc...|2016-01-01 04:35:47|040b6f5e595e576a|    /your-box/funnel|d6d4bd36-e7ee-4fa...|    /checkout/extras|
+--------------------+-------------------+----------------+--------------------+--------------------+--------------------+
only showing top 10 rows

# complete
```

* The execution of the job can be also monitored using the Spark Shell Web UI link: [http://localhost:4040](http://localhost:4040)  
	
	* The sample screenshots of Web UI output also available in the project directory **dsc004/docs** of this repo  



* The entire code is available for review in **dsc004/src/main/scala/DSC03.scala** file in this repo 

* the output of the job will be written to **E:\tmp\dsc03\out** directory


* The sample output in csv format is also available in the project directory **dsc004/resources/out** of this repo



