package com.sz.dsc004

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{Dataset, SparkSession}
import java.sql.Date
import java.sql.Timestamp
import java.io.File
import java.net.URL
import sys.process._

import org.apache.commons.io._

import com.typesafe.config.ConfigFactory
import configs.Configs

// import com.typesafe.config.ConfigFactory

object DSC03 {
  // name of class attributes have to be exactly same as as header in the file
  // otherwise the file will not be parsed
  // NB: import java.sql.Date for correct date type parsing
  case class DSC03_Schema(
                           event_id:String,
                           collector_tstamp:Timestamp,
                           domain_userid:String,
                           page_urlpath:String
                         )

  var sc: SparkContext = _

  var spark_master: String = _


  def main(args: Array[String]): Unit = {

    // uncomment for prod run
    // spark_master = args(0)
    // val inputFile = args(1)

    // just for debug purposes -- use the construct above for prod

    spark_master = "local[*]"
    println(":: Starting Spark 2.2.0 app - DSC Exercise #3")
    println(":: running in local mode: " + spark_master)

    val file_name = "pageViews.csv"
    val url = "https://dsc-data-challenge.s3-us-west-2.amazonaws.com/" + file_name

    val base_dir = "E:\\tmp\\dsc03"

    val input_dir = base_dir + "\\in"
    val output_dir = base_dir + "\\out"

    val inputFile = input_dir  + "\\"+ file_name


    println(":: source: " + url)
    println(":: setting up work directories: ")
    println(":: input directory: " + input_dir)
    println(":: output directory: " + output_dir)

    // setup work folders
    if (new File(input_dir).exists) {
      FileUtils.deleteDirectory(new File(input_dir))
    }

    if (new File(output_dir).exists) {
      FileUtils.deleteDirectory(new File(output_dir))
    }

    if (new File(base_dir).exists) {
      FileUtils.deleteDirectory(new File(base_dir))
    }

    new File(base_dir).mkdir()
    new File(input_dir).mkdir()
    //new File(output_dir).mkdir()

    // download file
    new URL(url) #> new File(inputFile) !!

    // disable excessive logging
    import org.apache.log4j.Logger
    import org.apache.log4j.Level
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    val app_name = "dsc excercise #3"

    println(":: define Spark context: ")
    // define the main spark entry points
    val conf = new SparkConf()
    conf.setAppName(app_name)
    conf.setMaster(spark_master)
    // object level: used by many object methods
    sc = new SparkContext(conf)

    // disable excessive logging
    sc.setLogLevel("WARN")

    // create Spark Session
    val ss: SparkSession = SparkSession.builder
      .master(spark_master)
      .enableHiveSupport() // hive dependency added to build.sbt
      .appName(app_name)
      .getOrCreate()

    // provides object encoder/serializer  for primitive data types
    // NB:! imported from object - not class
    import ss.implicits._

    println(":: define CSV input parsing to DataSet: ")
    // data set based on input file
    val dsc_03 = ss
      .read
      .option("header", "true") // Use first line of all files as header
      .option("delimiter", ",")  // CSV delimiter
      .option("dateFormat","MM/dd/yyyy")
      .option("inferSchema","true") // makes spark to make two passes on data: TODO: check how to avoid it
      .csv(inputFile)
      .as[DSC03_Schema] // convert to Dataset with schema defined in case class DSC03_Schema

    // define virtual table to use with Spark SQL
    val dsc_03_vw = dsc_03.createOrReplaceTempView("dsc_03_vw")

    println(":: define Spark SQL query transformation: ")
    // define Spark SQL query
    val dsc_linked_03_vw = ss.sql(
      s"""
         |SELECT
         |   event_id
         | , collector_tstamp
         | , domain_userid
         | , page_urlpath
         | , LEAD(event_id) OVER(PARTITION by domain_userid order by collector_tstamp) as next_event_id
         | , LEAD(page_urlpath) OVER(PARTITION by domain_userid order by collector_tstamp) as next_urlpath
         |FROM dsc_03_vw
         |ORDER BY
         |    domain_userid
         |  , collector_tstamp
      """.stripMargin // do not use () -- not sure why
    )

    println("::write output to destination directory: ")
    // write output to destination directory
    dsc_linked_03_vw
      .repartition(1) // save everything to one file
      .write
      .format("com.databricks.spark.csv")
      .option("header","true")
      .option("delimiter", ",")
      .save(output_dir)

    println("::show a few lines of the output: ")
    // show a few lines
    dsc_linked_03_vw.show(10)

  }
}